import {Tasks}from './tasks.jsx';

import React from 'react';
import Paper from 'material-ui/lib/paper';
import RaisedButton from 'material-ui/lib/raised-button';

export const Wrong = React.createClass({
    render(){
        const tryAgain = "/steg/"+this.props.step;
        var audioPath = 'audio/provigjen.mp4';
        var path =  document.location.origin + '/'+audioPath;
        new Media(path).play();
        return (
            <div style={{height: "100%", width: "100%", "backgroundImage": "url('/img/farger/rod.jpg')", backgroundSize: "cover" }}>
                <center><h1>Prøv igjen</h1></center>

                <a href={tryAgain}><RaisedButton label="Gå tilbake" fullWidth secondary/></a>
            </div>
        )
    }
})
