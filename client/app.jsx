import React from 'react';
import RaisedButton from 'material-ui/lib/raised-button';
import Card from 'material-ui/lib/card/card';
import CardActions from 'material-ui/lib/card/card-actions';
import CardHeader from 'material-ui/lib/card/card-header';
import CardMedia from 'material-ui/lib/card/card-media';
import CardTitle from 'material-ui/lib/card/card-title';
import CardText from 'material-ui/lib/card/card-text';
// import Link  from 'react-router';
import AppBar from 'material-ui/lib/app-bar';
import IconButton from 'material-ui/lib/icon-button';
import NavigationClose from 'material-ui/lib/svg-icons/navigation/close';
import IconMenu from 'material-ui/lib/menus/icon-menu';
import MoreVertIcon from 'material-ui/lib/svg-icons/navigation/more-vert';
import MenuItem from 'material-ui/lib/menus/menu-item';
import injectTapEventPlugin from 'react-tap-event-plugin';
import GridList from 'material-ui/lib/grid-list/grid-list';
import GridTile from 'material-ui/lib/grid-list/grid-tile';
import StarBorder from 'material-ui/lib/svg-icons/toggle/star-border';

injectTapEventPlugin();

// define and export our Layout component

export const Layout = React.createClass({
    render(){
        return (
            <div style={{ position:"fixed",
    padding:0,
    margin:0,

    top:0,
    left:0,

    width: "100%",
    height: "100%"}}>
            {this.props.content}
            </div>
        )
    }
});
var backroundMusic;
export const TitleScreen = React.createClass({
    render(){
        var stopMusicAndContinue;

        if(Meteor.isCordova) {
            console.log("Loading music");

            var audioPath = 'audio/intromusikk.mp3';
            var path = document.location.origin + '/' + audioPath;
            backgroundMusic = new Media(path);
            backgroundMusic.play();
            stopMusicAndContinue = function () {
                backgroundMusic.stop();
                FlowRouter.go('/steg/1');
            }
        }else{
            stopMusicAndContinue = function(){
                FlowRouter.go('/steg/1');
            }
        }

        return (
            <span>
            <a onTouchTap={stopMusicAndContinue}>
                <Card>
                <CardMedia

                >
                <img src="/img/Forsidebilde.jpg" />
                </CardMedia>
                    <CardActions>
                        <RaisedButton label="SPILL" fullWidth secondary/>
                    </CardActions>
                </Card>
            </a>
        </span>
        )
    }
})

// define and export our Welcome component
export const Welcome = React.createClass({
    getInitialState(){
        return {
            tiles: [
                {
                    title: 'Furu',
                    state: 'incomplete'
                },
                {
                    title: 'Bjørk',
                    state: 'incomplete'
                },
                {
                    title: 'Eik',
                    state: 'incomplete'
                },
                {
                    title: 'Lønn',
                    state: 'incomplete'
                }
            ],
            start: undefined,
            time: undefined,
            bestTime: undefined
        }
    },
    startGame(){
        if(this.state.start !== undefined)
            return;
        this.setState({
            start: moment().toDate(),
            time: 0
        })
        setInterval(this.gameUpdate, 1000);
    },
    stopGame(){
        clearInterval(this.gameUpdate);
        let bestTime = this.state.bestTime;
        if(this.state.bestTime === undefined || this.state.bestTime > this.state.time)
            bestTime = this.state.time;
        const newTiles = _.map(this.state.tiles, (t)=>{
            t.state = 'incomplete'
            return t;
        })
        this.setState({
            bestTime: bestTime,
            time: undefined,
            start: undefined,
            tiles: newTiles
        })
    },
    gameUpdate(){

        if(_.every(this.state.tiles, (t)=>{
            return t.state === 'complete';
        })){
            this.stopGame();
            return;
        }

        this.setState({time: this.state.time + 1})
    },
    handleTileClick(tile){
        this.startGame();
        let newTiles = [];
        if(tile.state === 'incomplete'){
            newTiles = _.map(this.state.tiles, (t)=>{
                if(t.state === 'active')
                    t.state = 'incomplete';
                if(t.title === tile.title)
                    t.state = 'active';
                return t;
            })
            this.setState({tiles: newTiles});
        }
        let self = this;
        cordova.plugins.barcodeScanner.scan(
                    function (result) {
                        if(tile.title === result.text)
                        {
                            alert("Riktig!");
                            newTiles = _.map(newTiles, (t)=>{
                                if(t.title === tile.title)
                                    t.state = 'complete';
                                return t;
                            })
                            self.setState({tiles: newTiles});
                        }
                        else
                            alert('feil');
                    },
                    function (error) {
                        alert("Scanning failed: " + error);
                    }
                );

    },
    renderTimerLabel(){
        if(this.state.time === undefined)
            return <span></span>
        let bestTime = this.state.bestTime !== undefined ? <h3>Beste tid: {this.state.bestTime} sekunder</h3> : <span></span>
        return (<div>
                    {bestTime}
                    <h3>{ this.state.time } sekunder</h3>
                </div>)
    },
    render(){
        return (
            <div>
            {this.renderTimerLabel()}
            <GridList
            cellHeight={200}
            >
            {this.state.tiles.map(tile => {
                let color = 'rgba(100,100,100,1)'
                if(tile.state === 'active')
                    color = 'rgba(0,150,180,1)'
                else if(tile.state === 'complete')
                    color = 'rgba(100,200,0,1)'
                return (
                <GridTile
                key={tile.img}
                title={tile.title}
                // subtitle={<span><b>{tile.author}</b></span>}
                // actionIcon={<IconButton><StarBorder color="white"/></IconButton>}
                titleBackground={color}
                >
                <img onTouchTap={this.handleTileClick.bind(this, tile)} src="/img/tree.jpg" />
                </GridTile>
            )})}
            </GridList>
            </div>
        )
    }
});
