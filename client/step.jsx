import {Tasks} from './tasks.jsx';
import React from 'react';
import Paper from 'material-ui/lib/paper';
import GridList from 'material-ui/lib/grid-list/grid-list';
import GridTile from 'material-ui/lib/grid-list/grid-tile';

export const Step = React.createClass({
    getInitialState(){
        return Tasks;
    },
    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },
    handleTileClick(tile){
        if(Meteor.isCordova) {
            var self = this;
            var audioPath = 'audio/'+tile.ting+'.m4a.mp4';
            var path =  document.location.origin + '/'+audioPath;
            new Media(path).play();
            cordova.plugins.barcodeScanner.scan(
                function (result) {
                    if(result.text === "")
                        return;
                    var res = self.capitalizeFirstLetter(result.text.toLowerCase());
                    if (tile.ting === res && tile.riktig) {
                        FlowRouter.go("/steg/"+self.props.step+"/riktig");
                    }
                    else {
                        FlowRouter.go("/steg/" + self.props.step + "/feil");
                    }
                },
                function (error) {
                    alert("Scanning failed: " + error);
                }
            );
        }else{
            console.log("Not cordova. Assuming debug and will assume proper QR")
            if(tile.riktig){
                FlowRouter.go("/steg/"+this.props.step+"/riktig")
            }else{
                FlowRouter.go("/steg/"+this.props.step+"/feil")
            }
        }
    },
    renderOptions(oppg){
        var key = 0;

        return oppg.alternativer.map(alternativ => {

            const imgUrl = "/img/obj/"+alternativ.ting+".jpg";
            return  (
                <div className="row" ><h1 style={{backgroundColor: "#FFF", marginBottom: "0px"}}>
                    {alternativ.ting}
                </h1>
                    <img style={{maxHeight: "150px", maxWidth: "150px"}} onTouchTap={this.handleTileClick.bind(this, alternativ)} src={imgUrl} />
                </div>
            );
        });
    },
    render(){
        var oppg = _.findWhere(this.state.oppgaver, {oppgaveNr: parseInt(this.props.step)});

        const mainImgUrl = "/img/obj/" + oppg.ting + ".jpg";
        const colorURL = "/img/farger/" + oppg.farge+ ".jpg";
        const audioURL = "/audio/"+oppg.ting+".m4a.mp4";

        var playMainSound = function(){
            var audioPath = 'audio/'+oppg.ting+'.m4a.mp4';
            var path =  document.location.origin + '/'+audioPath;
            new Media(path).play();
        }

        return (
            <div style={{ position:"fixed",
    padding:0,
    margin:0,

    top:0,
    left:0,
    backgroundImage: "url('"+colorURL+"')",
    backgroundSize: "cover",
    width: "100%",
    height: "100%"}}>

                <div className="col-xs-8" onTouchTap={playMainSound} >
                    <h1 style={{backgroundColor: "#FFF", marginBottom: "0px"}}>{oppg.ting}</h1>
                    <img style={{width: "300px"}} src={mainImgUrl} />
                </div>
                <div className="col-xs-4">

                    {this.renderOptions(oppg)}

                </div>
            </div>
        )
    }
})
