export const Tasks = {
    oppgaver: [
    {
        ting: "Katt",
        oppgaveNr: 1,
        farge: "bla",
        alternativer: [
            {
                ting: "Bok",
                riktig: false
            },
            {
                ting: "Lue",
                riktig: false
            },
            {
                ting: "Hatt",
                riktig: true
            }
        ]
    },
    {
        ting: "Mus",
        oppgaveNr: 2,
        farge: "gul",
        alternativer: [
            {
                ting: "Hus",
                riktig: true
            },
            {
                ting: "Sko",
                riktig: false
            },
            {
                ting: "Mose",
                riktig: false
            }
        ]
    },
    {
        ting: "Bil",
        oppgaveNr: 3,
        farge: "lilla",
        alternativer: [
            {
                ting: "Blomst",
                riktig: false
            },
            {
                ting: "Tog",
                riktig: false
            },
            {
                ting: "Pil",
                riktig: true
            }
        ]
    },
    {
        ting: "Sopp",
        oppgaveNr: 4,
        farge: "orangsje",
        alternativer: [
            {
                ting: "Suppe",
                riktig: false
            },
            {
                ting: "Kopp",
                riktig: true
            },
            {
                ting: "Kongle",
                riktig: false
            }
        ]
    },
    {
        ting: "Stein",
        oppgaveNr: 5,
        farge: "turkis",
        alternativer: [
            {
                ting: "Grein",
                riktig: true
            },
            {
                ting: "Stokk",
                riktig: false
            },
            {
                ting: "Pinne",
                riktig: false
            }
        ]
    },
    {
        ting: "Gris",
        oppgaveNr: 6,
        farge: "rosa",
        alternativer: [
            {
                ting: "Ku",
                riktig: false
            },
            {
                ting: "Gress",
                riktig: false
            },
            {
                ting: "Fis",
                riktig: true
            }
        ]
    }
]

}