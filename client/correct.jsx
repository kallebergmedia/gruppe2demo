import {Tasks}from './tasks.jsx';

import React from 'react';
import Paper from 'material-ui/lib/paper';
import RaisedButton from 'material-ui/lib/raised-button';

export const Correct = React.createClass({
    render(){


        if(parseInt(this.props.step) >= Math.max(..._.pluck(Tasks.oppgaver, 'oppgaveNr'))){
            return (
                <div style={{height: "100%", width: "100%", "backgroundImage": "url('/img/farger/gronn.jpg')", backgroundSize: "cover" }}>
                    <center><h1>Gratulerer</h1><h3>Dere klarte alle oppgavene!</h3><img src="/img/meta/ballonger.gif" /></center>
                </div>
            )
        }else{
            const nextStep = "/steg/"+(parseInt(this.props.step)+1);
            var audioPath = 'audio/riktig.mp4';
            var path =  document.location.origin + '/'+audioPath;
            new Media(path).play();
            return (

                <div style={{height: "100%", width: "100%", "backgroundImage": "url('/img/farger/green.jpg')", backgroundSize: "cover" }}>
                    <center><h1>Riktig</h1><h3>Dere klarte det</h3></center>

                    <a href={nextStep}><RaisedButton label="FORTSETT" fullWidth secondary/></a>
                </div>


            )
        }
    }
})
