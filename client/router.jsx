import React from 'react';
import {mount} from 'react-mounter';

// load Layout and Welcome React components
import {Layout, Welcome, TitleScreen} from './app.jsx';

import {Step} from './step.jsx';
import {Correct} from './correct.jsx';
import {Wrong} from './wrong.jsx';

FlowRouter.route("/", {
  action() {
    mount(Layout, {
        content: (<TitleScreen />)
    });
  }
});


FlowRouter.route("/steg/:step", {
    action(params, queryParams){
        mount(Layout, {
            content: (<Step step={params.step} />)
        });
    }
});

FlowRouter.route("/steg/:step/riktig", {
    action(params, queryParams){
        console.log("RIGHT");
        mount(Layout, {
            content: (<Correct step={params.step} />)
        });
    }
});

FlowRouter.route("/steg/:step/feil", {
    action(params, queryParams){
        console.log("WRONG");
        mount(Layout, {
            content: (<Wrong step={params.step} />)
        });
    }
});
